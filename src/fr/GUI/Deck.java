package fr.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class Deck extends JPanel {
    private Map<String, Card> buttons;
    private JButton exec_button;
    private String action;


    Deck() {
        setPreferredSize(new Dimension(865, 160));
        action = "";
    }

    void setDeck(Map<String, Integer> cards) {
        /*
        Display the cards
         */
        //JButton exec_button = new JButton("Exécuter");
        //exec_button.addActionListener(this);  // Nous ajoutons notre fenêtre à la liste des auditeurs de notre bouton
        //this.add(exec_button);
        buttons = new HashMap<>();
        exec_button = new JButton("Exécuter"); // On créé le bouton Exécuter
        exec_button.addActionListener(new BoutonListener());  // On ajoute la classe en auditeur du bouton
        this.add(exec_button);
        for (String key : new String[] {"avant", "gauche", "droite", "laser", "pierre", "glace"}) {
            if (cards.get(key) != 0) {
                buttons.put(key, new Card(key, cards.get(key)));
                buttons.get(key).addActionListener(new BoutonListener());
                this.add(buttons.get(key));
            }
        }
    }

    public String getAction() {
        System.out.println(action);
        return action;
    }

    public void lol() {
        System.out.println("Result : " + action);
    }

    //Classe écoutant notre premier bouton
    class BoutonListener implements ActionListener {
        //Redéfinition de la méthode actionPerformed()
        public void actionPerformed(ActionEvent arg0) {
            System.out.println("Entrée détectée");
            if (arg0.getSource() == buttons.get("avant")) {
                action = "add avant";
            }
            if (arg0.getSource() == buttons.get("droite"))
                action = "add droite";
            if (arg0.getSource() == exec_button) {
                action = "exec";
                lol();
            }
        }
    }
}
