package fr.GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.awt.event.ActionEvent;

public class Window extends JFrame {
    CardLayout fenetres = new CardLayout();
    JPanel fenetre = new JPanel();
    String[] listeFenetres = {"MENU", "JEU"};
    private Board board;
    private Deck deck;
    JPanel menu;
    JPanel jeu;

    public Window() throws IOException {
        /*
        Initialisation des classes
        Appel de la fonction loop
        */

        // On configure la fenêtre
        setTitle("Robot Turtles");  // Définit le titre de la fenêtre
        setSize(700, 800);
        setLocation(400, 0);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  // Terminer le processus lorsque la croix rouge est cliquée
        //setResizable(false); // Empêcher le recadrement de la fenêtre
        setAlwaysOnTop(true);  // Garder la fenêtre au premier plan
        setVisible(true);  // Mettre la fenêtre au premier plan
        //setBackground(Color.black);
        this.setContentPane(new Panneau());
        JButton bouton = new JButton("Démarrer");
        this.getContentPane().add(bouton);

        // On créée les fenêtres principales du programme
        fenetre.setLayout(fenetres);
        menu = new JPanel();
        jeu = new JPanel();
        board = new Board();  // On créé le plateau de jeu en fonction du mode de jeu
        deck = new Deck();
        jeu.add(board, BorderLayout.CENTER);  // On inclue le panel à la fenêtre (Frame)
        jeu.add(deck, BorderLayout.SOUTH);  // On inclue le panel à la fenêtre (Frame)
        fenetre.add(menu, listeFenetres[0]);
        fenetre.add(jeu, listeFenetres[1]);
        //setContentPane(fenetre);

        fenetres.show(fenetre, listeFenetres[0]); // On affiche le menu en premier

        System.out.println("Window successfully created");
    }


    class Panneau extends JPanel {
        public void paintComponent(Graphics g){
            try {
                Image img = ImageIO.read(new File("background.jpeg"));
                //Pour une image de fond
                g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Font font = new Font("Arial", Font.BOLD, 40);
            g.setFont(font);
            g.setColor(Color.black);
            g.drawString("Bienvenue dans notre jeu", 120, 300);
            g.drawString("Robot Turtles !", 250, 360);
            Font font2 = new Font("Arial", Font.BOLD, 20);
            g.setFont(font2);
            g.setColor(Color.white);
            g.drawString("Jeu créé par :", 10, 25);
            g.drawString("Benjamin Da Cruz", 10, 50);
            g.drawString("Thibault de Courson", 10, 75);
            g.drawString("Clément Pujo", 10, 100);
            Font font3 = new Font("Arial", Font.BOLD, 8);
            g.setFont(font3);
            g.setColor(Color.white);
            g.drawString("Cliquez sur démarrer pour lancer une partie", 275, 40);
        }
    }

    public void displayGame(int gamemode) throws IOException {
        //deck = new Deck(cards);  // On créé le deck du joueur
        fenetres.show(fenetre, listeFenetres[1]); // On affiche le jeu
        //board.init(gamemode);
        System.out.println("Lancement du jeu");
    }

    public void displayCards(Map<String, Integer> cards) {
        //deck = new Deck(cards);  // On créé le deck du joueur

        System.out.println("Test work");
    }

    public void displayBoard(int gamemode, Map<String, Integer> cards) throws IOException {
        //setVisible(true);  // Mettre la fenêtre au premier plan
    }

    public String getAction() {
        deck.lol();
        String test = deck.getAction();
        System.out.print("");
        return test;
    }

    public void update() {
        board.repaint();
    }
}
