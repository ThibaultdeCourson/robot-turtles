package fr.GUI;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Board extends JPanel {
    private final char[][] plateau = new char[8][8];
    //private static ArrayDeque<Ruby> rubies;
    //Image[] players = {null, null, null, null};
    //private static ArrayList<Image> players = new ArrayList<Image>(); test
    BufferedImage ruby = null;
    Image wall = null;
    private int gamemode;

    Board() {
    }

    public void init(int gamemode) throws IOException {
        this.gamemode = gamemode;
        System.out.println("Initialisation des objets");

        // On initialise les rubis
        ruby = ImageIO.read(new File("data/rubyr.png"));
        // On initialise les rubies
        //rubies = new ArrayDeque<>();
        if (gamemode < 2) {
            plateau[3][0] = 'R';
            //rubies.add(new Ruby(3));
        }
        if (gamemode > 0) {
            plateau[6][0] = 'R';
            plateau[gamemode - 1][0] = 'R';
            //rubies.add(new Ruby(6));
            //rubies.add(new Ruby(gamemode - 1));
        }

        // On initialise les murs
        wall = ImageIO.read(new File("data/wallr.png"));
    }

    public void paintComponent (Graphics g) {
        g.setColor(Color.white);  //On choisit une couleur de fond pour le rectangle
        if (gamemode == 2)
            g.fillRect(0, 0, this.getWidth(), this.getHeight()); //On le remplit
        else
            g.fillRect(0, 0, 700, this.getHeight()); //On le remplit
        /*
        for (int i = 0; i < gamemode + 2; i++)
            g.drawImage(players.get(i).getSprite(), players.get(i).getX()*100, players.get(i).getY()*100, this);
        */
        if (gamemode < 2) {
            for (int i = 0; i < 8; i++)
                g.drawImage(wall, 700, i * 100, this);
            g.drawImage(ruby, 300, 0, this);
        }
        if (gamemode > 0) {
            g.drawImage(ruby, 600, 0, this);
            g.drawImage(ruby, (gamemode - 1) * 100, 0, this);
        }
    }

    public void draw(Graphics g, BufferedImage image, int x, int y) {
        g.drawImage(ruby, x-image.getWidth()/2, y-image.getHeight()/2, this);
    }
}
