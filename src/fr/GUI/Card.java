package fr.GUI;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;

public class Card extends JButton implements MouseListener{
    private String name;
    private Image img;
    private Image imgRepos;
    private Image imgSurvole;
    private Image imgPresse;
    private int number;

    public Card(String cardType, int number) {
        super(cardType);
        this.number = number;
        this.setPreferredSize(new Dimension(110, 150));
        this.name = cardType;
        try {
            imgRepos = ImageIO.read(new File("data/cards/" + cardType + ".png"));
            imgSurvole = imgRepos; //ImageIO.read(new File("data/cards/" + cardType + "Survole.png"));
            imgPresse = imgRepos; //ImageIO.read(new File("data/cards/" + cardType + "Presse.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        img = imgRepos;
        this.addMouseListener(this);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        GradientPaint gp = new GradientPaint(0, 0, Color.blue, 0, 20, Color.cyan, true);
        g2d.setPaint(gp);
        //g2d.drawImage(img, 0, 0, this);
        g2d.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
        g2d.setColor(Color.black);

        if (number > 1) {
            g2d.setFont(new Font("Courier", Font.BOLD, 50));
            FontMetrics fm = g2d.getFontMetrics(); // Objet permettant de connaître les propriétés d'une police (taille)
            g2d.drawString("x" + number, this.getWidth() - fm.stringWidth("x" + number), 2*fm.getHeight()/3);
        }
    }

    public void mouseClicked(MouseEvent event) {
        //Inutile d'utiliser cette méthode ici
    }

    public void mouseEntered(MouseEvent event) {
        //Nous changeons le fond de notre image lors du survol
        img = imgSurvole;
    }

    public void mouseExited(MouseEvent event) {
        //Nous changeons le fond de notre image lorsque nous quittons le bouton
        img = imgRepos;
    }

    public void mousePressed(MouseEvent event) {
        //Nous changeons le fond de notre image lors du clic gauche
        img = imgPresse;
    }

    public void mouseReleased(MouseEvent event) {
        //Nous changeons le fond de notre image lorsque nous relâchons le clic si la souris est toujours sur le bouton
        if((event.getY() > 0 && event.getY() < this.getHeight()) && (event.getX() > 0 && event.getX() < this.getWidth())){
            img = imgSurvole;
        }
        //Si on se trouve à l'extérieur, on dessine le fond par défaut
        else{
            img = imgRepos;
        }
    }
}