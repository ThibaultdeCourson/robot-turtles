package fr.CLI;

import java.util.Arrays;
import java.util.Scanner;

public class Input {
    private static Scanner scanner = new Scanner(System.in);

    public static int choixMenu() {
        int choix;
        do {
            System.out.print("Entrez votre choix : ");
            choix = scanner.nextInt();
        } while (choix < 2 || choix > 6);
        scanner.nextLine();
        System.out.println("\n");
        return choix;
    }

    public static String choixAction(String options) {
        String mot;
        if (options.equals("normal")) {
            do {
                System.out.print("Entrez 1 pour exécuter le programme ou entrez le nom d'une carte pour l'utiliser : ");
                mot = scanner.nextLine();
                if (Arrays.asList("avant", "gauche", "droite", "laser").contains(mot)) mot = "2 " + mot;
                else if (Arrays.asList("pierre", "glace").contains(mot)) mot = "3 " + mot;
                else if (!mot.equals("1")) mot = null;
            } while (mot == null);
        } else {
            do {
                if (options.equals("discard")) System.out.print("Entrez le nom d'une carte à défausser ou pressez ENTER pour valider : ");
                else System.out.print("Entrez le nom d'une autre carte à ajouter au programme ou pressez ENTER pour valider : ");
                mot = scanner.nextLine();
                if (!Arrays.asList("avant", "gauche", "droite", "laser", "").contains(mot)) mot = null;
            } while (mot == null);
        }
        return mot;
    }

    public static int[] chooseTarget(String card) {
        int x;
        int y = 0;
        do {
            System.out.print("Entrez l'absisse (x) de la case visée ou 0 pour annuler : ");
            x = scanner.nextInt();
        } while (x < 0 || x > 8);
        if (x != 0) {
            do {
                System.out.print("Entrez l'ordonnée (y) de la case visée ou 0 pour annuler : ");
                y = scanner.nextInt();
            } while (y < 0 || y > 8);
        }
        scanner.nextLine();
        if (x == 0 || y == 0)
            System.out.println("Carte mur de " + card + " désélectionnée");
        return new int[]{x - 1, 8 - y};
    }
}
