package fr.CLI;

import java.util.Map;

public class Output {
    public static void displayBoard(String type, String[][] plateau) {
        StringBuilder absisses = new StringBuilder("\nPlateau de jeu :\n");
        for (int i = 1; i < 9; i++) absisses.append("             ").append(i);
        absisses.append("\n    ").append("_".repeat(14*8 + 3));
        System.out.println(absisses);
        for (int y = 7; y >= 0; y--) {
            System.out.print((8-y) + "  |   ");
            for (int x = 0; x < 8; x++) {
                String element = plateau[x][y];
                if (element == null) element = "      -       ";
                else if (element.equals("|")) element = "     |        ";
                else if (element.equals("-")) element = "--------------";
                else element = "[" + element + "]" + " ".repeat(12 - element.length());
                System.out.print(element);
            }
            System.out.println();
        }

        // Si plusieurs affichages vont s'enchainer attendre un peu
        if (type.equals("temp")) waitABit(3);
        System.out.println("\n");
    }

    public static void displayCards(String type, Map<String, Integer> cards) {
        String[] elements;
        if (type.equals("deck")) {
            System.out.println("Jeu :");
            elements = new String[] {"avant", "gauche", "droite", "laser", "pierre", "glace"};
        } else {
            elements = new String[] {"avant", "gauche", "droite", "laser"};
            // On compte le nom de cartes piochées
            int nbreCards = 0;
            for (String nomCarte : elements) nbreCards += cards.get(nomCarte);

            if (nbreCards == 1) System.out.print("Carte piochée : ");
            else System.out.print("Cartes piochées : ");

        }

        for (String nomCarte : elements) {
            int nbreCartes = cards.get(nomCarte);
            if (nbreCartes > 1) System.out.print(nbreCartes + "x ");
            if (nbreCartes > 0) System.out.print("[" + nomCarte + "]  ");
        }
        System.out.println("\n");
    }

    public static void displayMenu() {
        System.out.println("\n --- ROBOT TURTLES ---\nJeu développé par Thibault de Courson, Clément Pujo et " +
                "Benjamin Da Cruz\n\n[2] - 2 joueurs\n[3] - 3 joueurs\n[4] - 4 joueurs\n[5] - Passer en interface " +
                "graphique\n[6] - Quitter\n");
    }

    public static void wallBuilt(String action, int[] target) {
        System.out.println("Carte mur de " + action + " posée en [" + (target[0] + 1) + "; " + (8 - target[1]) + "]");
    }

    private static void waitABit(double time) {
        try {
            Thread.sleep((int)(time*1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void message(String action) {
        switch (action) {
            case "laser ruby":
                System.out.println("Votre laser a été réfléchi par un rubis");
                break;
            case "complete program":
                System.out.println("Programme complété\n");
                break;
            case "start program":
                System.out.println("\nLancement de votre programme...");
                break;
            case "end program":
                System.out.println("Programme exécuté\n");
                break;
            case "collision":
                System.out.println("Vous avez percuté un mur");
                break;
            case "blocking":
                System.out.println("Ce mur bloque l'accès à un rubis vous ne pouvez pas le placer");
                break;
            case "end turn":
                System.out.println("[FIN DU TOUR]");
                waitABit(2);
                for (int i = 0; i < 20; i++) {
                    waitABit(0.1);
                    System.out.println("\n");
                }
                break;
            case "end":
                System.out.println("Fin du jeu !\n\nClassement des joueurs :");
                break;
        }
    }

    public static void message(String action, String card) {
        switch (action) {
            case "mur":
                System.out.println("\nVous avez choisi d'utiliser une carte mur de " + card);
                break;
            case "laser":
                System.out.println("Votre laser a rencontré un" + card);
                break;
            case "collision":
                System.out.println("Vous avez percuté un" + card);
                break;
            case "card":
                System.out.println("Carte [" + card + "] utilisée");
                break;
            case "add":
                System.out.println("Carte [" + card + "] ajoutée à votre programme");
                break;
            case "discard":
                System.out.println("Carte [" + card + "] défaussée");
                break;
            case "not in deck":
                System.out.println("Vous n'avez pas de carte [" + card + "]");
                break;
            case "occupied":
                System.out.println("Cette case est occupée par [" + card + "] vous ne pouvez pas y bâtir de mur");
                break;
        }
    }

    public static void message(int position, int id) {
        System.out.println((position + 1) + " : Joueur " + (id + 1));
    }

    public static void message(String part, int player) {
        switch (part) {
            case "start turn":
                System.out.println("[TOUR DU JOUEUR " + (player + 1) + "]");
                break;
            case "collision":
                System.out.println("Vous avez percuté le joueur " + (player + 1));
                break;
            case "touché":
                System.out.println("Vous avez touché le joueur " + (player + 1));
                break;
            case "victory":
                System.out.println("Le joueur " + player + " a gagné !");
                message("end turn");
                break;
        }
    }
}
