package fr.main;

import fr.main.Actions.Avant;
import fr.main.Actions.Execute;
import fr.main.Actions.Laser;
import fr.main.Actions.Wall;

import java.util.ArrayList;

public class Turn {
    static boolean turn(int gamemode, ArrayList<Cards> players, String[][] plateau, Cards player) {

        // On affiche le plateau
        Interface.displayBoard("stat", plateau);

        // On fait jouer le joueur
        int action = 0;
        int nbreAction = 0;
        String card;
        String state = "normal";
        boolean displayCards = true;
        //Player player = players.get(idPlayer);  // On récupère le joueur qui a la main
        do {
            // En fonction de la situation on affiche ou pas le jeu de cartes
            if (displayCards) Interface.displayCards("deck", player.getDeck());

            // On propose au joueur de choisir son action en fonction de la situation
            card = Interface.choixAction(state);

            if (state.equals("normal")) {
                // Si on n'est pas en train de construire le programme récupérer le type d'action choisi
                action = Character.getNumericValue(card.charAt(0));
                if (action == 1) card = "";
                else card = card.substring(2);
                state = "";
            }

            // Si l'utilisateur veut utiliser une carte...
            if (action != 1) {
                if (card.equals("")) {
                    // Si le joueur a fini de compléter son programme on met fin à la boucle
                    Interface.message("complete program");
                    state = "";
                } else {
                    // Vérification de la présence de la carte dans le jeu
                    if (player.inDeck(card)) {
                        if (action == 2) {
                            // On ajoute la carte au programme
                            player.addInstruction(card);
                            Interface.message("add", card);
                            // On compte le nombre de cartes que le joueur a utilisé
                            nbreAction++;
                            state = "add";
                            displayCards = true;
                        } else {
                            // On place un mur
                            if (!Wall.wall(plateau, player, card)) {
                                // Si l'utilisateur veut changer d'action
                                state = "normal";
                                displayCards = false;
                            }
                        }
                    } else {
                        // Si la carte n'est pas dans le jeu on l'indique
                        Interface.message("not in deck", card);
                        // Et on invite le joueur à choisir une autre option
                        displayCards = false;

                        // On ne permet à l'utilisateur de changer de type d'action que s'il essayait de poser un mur
                        if (action == 3 || !state.equals("add")) state = "normal";
                    }
                }
            }

                /* On redemande au joueur d'entrer un nouveau nom de carte si :
                - La carte qu'il a demandé n'est pas dans son jeu
                - Il est en train de remplir son programme, qu'il n'a pas demandé d'arrêter et qu'il n'a pas placé ses
                  cinq cartes
                */
        } while (!state.equals("") && nbreAction < 5);

        if (action == 1) {
            Interface.message("start program");
            if (executeProgram(gamemode, players, plateau, player)) {
                Interface.message("victory");
                return true;
            }
        }

        if (nbreAction < 5) {
            displayCards = true;
            do {
                // En fonction de la situation on affiche ou pas le jeu de cartes
                if (displayCards) Interface.displayCards("deck", player.getDeck());

                // On propose au joueur de choisir son action
                card = Interface.choixAction("discard");

                // Si l'utilisateur veut utiliser une carte...
                if (!card.equals("")) {
                    // Vérification de la présence de la carte dans le jeu
                    if (player.inDeck(card)) {
                        player.discardCard(card);
                        // On compte le nombre de cartes que le joueur a utilisé
                        nbreAction++;
                        Interface.message("discard", card);
                    } else {
                        // Si la carte n'est pas dans le jeu on l'indique
                        Interface.message("not in deck", card);
                        // Et on invite le joueur à choisir une autre option
                        displayCards = false;
                    }
                }

                /* On redemande au joueur d'entrer un nouveau nom de carte si :
                - La carte qu'il a demandé n'est pas dans son jeu
                - Il est en train de remplir son programme, qu'il n'a pas demandé d'arrêter et qu'il n'a pas placé ses
                  cinq cartes
                */
            } while (!card.equals("") && nbreAction < 5);
        }

        if (nbreAction > 0) Interface.displayCards("drawn", player.fillDeck());

        // On affiche le message de fin de tour
        Interface.message("end turn");
        return false;
    }

    static boolean executeProgram(int gamemode, ArrayList<Cards> players, String[][] plateau, Cards player) {
        String card;
        boolean endgame = false;
        do {
            card = player.execProg();

            if (card != null) {
                Interface.message("card", card);

                // Si la file d'instruction n'est pas vide
                if (card.equals("avant")) endgame = Avant.avant(players, plateau, player);
                else if (!card.equals("laser")) Execute.setOrientation(plateau, player, card); // Si la carte est une carte [gauche] ou [droite]

                // On affiche le plateau mis à jour
                if (card.equals("laser")) Interface.displayBoard("temp", Laser.laser(gamemode, players, plateau, player));
                else Interface.displayBoard("temp", plateau);

                // Si un joueur a rencontré un diamant mettre fin à son tour
                if (endgame) {
                    plateau[player.getX()][player.getY()] = null;
                    Interface.message("victory");
                    return true;
                }
            } else {
                Interface.message("end program");
            }
        } while (card != null);
        return false;
    }
}