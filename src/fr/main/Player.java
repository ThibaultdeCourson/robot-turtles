package fr.main;

import java.util.*;

public class Player {
    private final int id;
    private boolean isPlaying = true;
    private int[] position = new int[2];
    private final int[] positionDepart = new int[2];
    private char orientation = 'S';
    private static final Map<Character, char[]> turn = new HashMap<>() {{
        put('N', new char[]{'W', 'E', 'S'});
        put('S', new char[]{'E', 'W', 'N'});
        put('W', new char[]{'S', 'N', 'E'});
        put('E', new char[]{'N', 'S', 'W'});
    }};

    public Player(int gamemode, int number) {
        id = number;
        /*
        On initialise le joueur
         */

        // On initialise la position du joueur
        Map<Integer, int[]> positions = new HashMap<>() {{  // Position de chaque joueur en fonction du mode de jeu
            put(0, new int[]{1, 0, 0});
            put(1, new int[]{5, 3, 2});
            put(2, new int[]{-1, 6, 5});
            put(3, new int[]{-1, -1, 7});
        }};
        position[0] = positions.get(id)[gamemode];
        position[1] = 7;
        positionDepart[0] = position[0];
        positionDepart[1] = position[1];
    }

    boolean getIsPlaying() {
        return isPlaying;
    }

    public void setIsPlaying() {
        isPlaying = false;
    }

    int getId() {
        return id;
    }

    public int[] getPositionDepart() {
        return positionDepart;
    }

    public int getX(){
        return position[0];
    }

    public int getY(){
        return position[1];
    }

    public void setPosition(int[] newPosition) {
        position[0] = newPosition[0];
        position[1] = newPosition[1];
    }

    public char setOrientation(String card) {
        int direction;
        // 0 pour gauche, 1 pour droite, 3 pour demi tour
        if (card.equals("reset")) {
            return orientation = 'S';
        } else {
            if (card.equals("gauche")) direction = 0;
            else if (card.equals("droite")) direction = 1;
            else direction = 2;  // Si le joueur a percuté un mur

            // On modifie l'orientation du personnage en fonction de la carte
            char newOrientation = turn.get(orientation)[direction];
            orientation = newOrientation;
            return newOrientation;
        }
    }

    public char getOrientation() {
        return orientation;
    }
}
