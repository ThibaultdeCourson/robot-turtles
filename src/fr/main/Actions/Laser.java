package fr.main.Actions;

import fr.main.Cards;
import fr.main.Interface;
import fr.main.Player;

import java.util.ArrayList;

public class Laser extends Execute {
    public static String[][] laser(int gamemode, ArrayList<Cards> players, String[][] plateau, Player player) {
        // On fait une copie du tableau pour afficher le rayon laser
        String[][] temp = new String[8][8];
        for (int x = 0; x < 8; x++)
            System.arraycopy(plateau[x], 0, temp[x], 0, 8);

        // Si la carte est une carte [laser]
        char direction = player.getOrientation();
        int x = player.getX();
        int y = player.getY();
        int i;
        boolean end = false;
        if (direction == 'N' || direction == 'S')
            i = y;
        else
            i = x;
        do {
            // En fonction de l'orientation on incrémente ou décrémente i
            if (direction == 'N' || direction == 'E')
                i++;
            else
                i--;
            // En fonction de l'orientation on attribue i à x ou y
            if (direction == 'N' || direction == 'S')
                y = i;
            else
                x = i;

            // On vérifie que la case ne se trouve pas en dehors du plateau
            if (i < 8 && i >= 0) {
                // On regarde ce que contient la case
                if (plateau[x][y] == null) {
                    // Si la case est vide
                    if (direction == 'N' || direction == 'S')
                        temp[x][y] = "|";
                    else
                        temp[x][y] = "-";
                } else {
                    // Si la case n'est pas vide
                    end = true;
                    String target = plateau[x][y];
                    if (target.equals("rubis")) {
                        // Si la case contient un rubis
                        if (gamemode == 0) {
                            setOrientation(plateau, player, "demi tour");
                        } else {
                            movePlayer(plateau, player, player.getPositionDepart());
                            setOrientation(plateau, player, "reset");
                        }
                        Interface.message("laser ruby");
                    } else if (target.substring(1, 5).equals(" Jou")) {
                        // Si la case contient un joueur
                        int ipCible = Character.getNumericValue(target.charAt(9)) - 1;
                        Player cible = players.get(ipCible);
                        if (gamemode == 0) {
                            setOrientation(plateau, cible, "demi tour");
                        } else {
                            movePlayer(plateau, cible, cible.getPositionDepart());
                            setOrientation(plateau, cible, "reset");
                        }
                        Interface.message("touché", ipCible);
                    } else if (target.equals("caisse")) {
                        // Si la case contient une caisse
                        Interface.message("laser", "e caisse");
                    } else {
                        // Si la case contient un mur
                        if (target.equals("glace"))
                            plateau[x][y] = null;
                        Interface.message("laser", (" mur de " + target));
                    }
                }
            } else {
                Interface.message("laser", " mur");
                end = true;
            }
        } while (!end);

        // Si le laser n'a rencontré aucun obstacle...
        return temp;
    }
}
