package fr.main.Actions;

import fr.main.Cards;
import fr.main.Interface;

public class Wall {
    public static boolean wall(String[][] plateau, Cards player, String card) {
        Interface.message("mur", card);
        int[] coord;
        boolean valide;
        String target;
        do {
            coord = Interface.chooseTarget(card);
            if (coord[0] == -1 || coord[1] == 8) {
                // Si l'utilisateur veut changer d'action
                return false;
            } else {
                // Si l'utilisateur a entré les coordonnées d'une case
                // On récupère le contenu de la case visée
                target = plateau[coord[0]][coord[1]];
                // On vérifie que la case visée est vide
                valide = target == null;
                if (valide) {
                    // Si la case ne bloque pas un joueur ou un rubis
                    valide = Pathfinder.init(plateau, coord);
                    if (valide) {
                        // Si la case est vide on place l'objet
                        plateau[coord[0]][coord[1]] = card;
                        player.buildWall(card);
                        Interface.wallBuilt(card, coord);
                    } else {
                        // Si la case n'est pas vide on demande au joueur de réessayer
                        Interface.message("blocking");
                    }
                } else {
                    // Si la case n'est pas vide on demande au joueur de réessayer
                    Interface.message("occupied", target);
                }
            }
        } while (!valide);
        return true;
    }
}
