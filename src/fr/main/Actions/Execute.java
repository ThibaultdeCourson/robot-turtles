package fr.main.Actions;

import fr.main.Player;
import java.util.HashMap;
import java.util.Map;

public class Execute {
    private static final Map<Character, Character> playerDirection = new HashMap<>() {{
        put('N', '^');
        put('S', 'V');
        put('W', '<');
        put('E', '>');
    }};

    static void movePlayer(String[][] plateau, Player player, int[] newPosition) {
        // On n'effectue le déplacement que si la nouvelle position est différente de la première
        if (!(newPosition[0] == player.getX() && newPosition[1] == player.getY())) {
            // On déplace le personnage sur le plateau
            plateau[newPosition[0]][newPosition[1]] = plateau[player.getX()][player.getY()];
            plateau[player.getX()][player.getY()] = null;

            // On enregistre la nouvelle position
            player.setPosition(newPosition);
        }
    }

    public static void setOrientation(String[][] plateau, Player cible, String card) {
        char symbole = playerDirection.get(cible.setOrientation((card)));
        // On remplace le symbole représentant l'orientation du personnage dans le tableau
        plateau[cible.getX()][cible.getY()] = symbole + plateau[cible.getX()][cible.getY()].substring(1);
    }
}
