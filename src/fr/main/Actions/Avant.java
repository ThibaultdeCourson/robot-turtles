package fr.main.Actions;

import fr.main.Cards;
import fr.main.Interface;
import fr.main.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Avant extends Execute{
    private static final Map<Character, int[]> move = new HashMap<>() {{
        put('N', new int[]{0, 1});
        put('S', new int[]{0, -1});
        put('W', new int[]{-1, 0});
        put('E', new int[]{1, 0});
    }};

    public static boolean avant(ArrayList<Cards> players, String[][] plateau, Player player) {
        // Si la carte est une carte [avant]
        int[] newPosition = new int[] {player.getX() + move.get(player.getOrientation())[0], player.getY() + move.get(player.getOrientation())[1]};
        if (newPosition[0] < 0 || newPosition[0] > 7 || newPosition[1] < 0 || newPosition[1] > 7) {
            // S'il rencontre un mur faire demi tour
            setOrientation(plateau, player, "demi tour");
            Interface.message("collision");
        } else {
            String target = plateau[newPosition[0]][newPosition[1]];
            if (target == null) {
                // Si la case est vide on déplace le joueur
                movePlayer(plateau, player, newPosition);
            } else if (target.equals("rubis")) {
                // Si la case est occupée par un rubis on affiche le déplacement puis sonne la fin du jeu
                player.setIsPlaying();
                movePlayer(plateau, player, newPosition);
                return true;
            } else if (target.substring(1, 5).equals(" Jou")) {
                // Si la case est occupée par un autre joueur on renvoie le joueur à sa position initiale
                movePlayer(plateau, player, player.getPositionDepart());
                setOrientation(plateau, player, "reset");
                // Ainsi que le joueur qu'il a percuté
                int ipCible = Character.getNumericValue(target.charAt(9)) - 1;
                Player cible = players.get(ipCible);
                movePlayer(plateau, cible, cible.getPositionDepart());
                setOrientation(plateau, cible, "reset");
                Interface.message("collision", ipCible);
            } else if (target.equals("caisse")) {
                setOrientation(plateau, player, "demi tour");
                Interface.message("collision", "e caisse");
            } else {
                // Si la case est occupée par un autre obstacle on fait faire demi tour au joueur
                setOrientation(plateau, player, "demi tour");
                Interface.message("collision", (" mur de " + target));
            }
        }
        return false;
    }
}
