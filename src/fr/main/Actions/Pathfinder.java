package fr.main.Actions;

import java.util.*;

class Pathfinder {
    static private Map<Integer, Boolean> statutNoeud;
    static private Map<Integer, int[]> noeudVoisins = new HashMap<>();
    static private int elements;

    static boolean init(String[][] plateau, int[] coord) {
        statutNoeud = new HashMap<>();
        int[][] orgPlateau = new int[8][8];
        int number = 1;
        elements = 0;

        // Créer un tableau des numéros des cases vides
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                String target = plateau[x][y];
                if (!(coord[0] == x && coord[1] == y)) {
                    if (target == null || target.equals("glace")) {
                        // Si la case est vide lui attribuer un numéro positif
                        orgPlateau[x][y] = number;
                        number++;
                    } else if (target.equals("rubis") || target.substring(1, 5).equals(" Jou")) {
                        // Si la case contient un rubis ou un joueur lui attribuer un numéro négatif
                        elements++;
                        orgPlateau[x][y] = -elements;
                    }
                }
            }
        }

        // Créer le dictionnaire des cases voisines de chaque chaque case
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (orgPlateau[x][y] != 0) {
                    ArrayList<Integer> voisins = new ArrayList<>();
                    // Si la case est vide lui ajouter ses voisins
                    if (x - 1 >= 0) if (orgPlateau[x - 1][y] != 0) voisins.add(orgPlateau[x - 1][y]);
                    if (x + 1 < 8) if (orgPlateau[x + 1][y] != 0) voisins.add(orgPlateau[x + 1][y]);
                    if (y - 1 >= 0) if (orgPlateau[x][y - 1] != 0) voisins.add(orgPlateau[x][y - 1]);
                    if (y + 1 < 8) if (orgPlateau[x][y + 1] != 0) voisins.add(orgPlateau[x][y + 1]);
                    noeudVoisins.put(orgPlateau[x][y], voisins.stream().mapToInt(i->i).toArray());
                }
            }
        }

        explorerEspace();
        return elements <= 1;
    }

    static private void explorerEspace() {
        Set<Integer> noeudsATester = new HashSet<>();

        // On enregistre les noeuds à tester
        for (int noeud : noeudVoisins.keySet()) {
            noeudsATester.add(noeud);
            statutNoeud.put(noeud, false);
        }

        int noeudAExplorer = -1;
        do {
            // Le noeud est en train d'être traité, on le retire de la liste des noeuds à traiter
            noeudsATester.remove(noeudAExplorer);

            // On prend un noeud exploré et on explore ses noeuds voisins
            for (int noeudVoisin : noeudVoisins.get(noeudAExplorer)) {
                if (noeudVoisin > 0) {
                    // Si la case voisine est vide la marquer comme explorée
                    statutNoeud.put(noeudVoisin, true);
                } else if (noeudsATester.contains(noeudVoisin)) {
                    // Si la case voisine n'est pas vide la supprimer et enregistrer sa rencontre
                    noeudsATester.remove(noeudVoisin);
                    elements--;
                }
            }

            noeudAExplorer = explorerNoeud(noeudsATester);
        } while (noeudAExplorer != 0);
    }

    static private int explorerNoeud(Set<Integer> noeuds) {
        for (int noeud : noeuds) if (statutNoeud.get(noeud)) return noeud;
        return 0;
    }
}
