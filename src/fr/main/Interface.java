package fr.main;
import fr.CLI.Input;
import fr.CLI.Output;
import fr.GUI.Window;
// import fr.GUI.Window;
import java.io.IOException;
import java.util.Map;

public class Interface {
    static private String type = "CLI";
    //private Window win;

    static void switchInterface() {
        /*
        Pour passer de l'interface CLI à l'interface GUI
         */
        if (type.equals("GUI")) type = "CLI";
        else type = "GUI";
    }

    static void initialisation() throws IOException {
        /*
        Initialisation de l'interface
        (création de la fenêtre graphique)
         */
        if (type.equals("GUI")) {
            System.out.println("Not implemented yet");
            Window win= new Window();
            //win.displayGame(gamemode);
            //win.displayBoard(gamemode, players.get(0).getDeck());
            //win.displayCards(players.get(0).getDeck());
            //win.repaint();
        } else {
            System.out.println("Interface en lignes de commandes lancée");
        }
    }

    static int choixMenu() {
        /*
        Demande à l'utilisateur de choisir une option du menu
         */
        if (type.equals("GUI")) return 0;
        else return Input.choixMenu();
    }

    static void displayMenu() {
        /*
        Affiche le menu
         */
        if (type.equals("GUI")) System.out.println("Not implemented yet");
        else Output.displayMenu();
    }

    static void displayBoard(String type, String[][] plateau) {
        /*
        Affiche (CLI) / met à jour (GUI) le plateau de jeu
         */
        if (type.equals("GUI")) System.out.println("Not implemented yet");
        else Output.displayBoard(type, plateau);
    }

    static void displayCards(String type, Map<String, Integer> cards) {
        /*
        Affiche des cartes :
        - Le jeu du joueur si type = "deck"
        - Les cartes piochées par le joueur sinon
         */
        if (type.equals("GUI")) System.out.println("Not implemented yet");  //win.displayCards(cards);
        else Output.displayCards(type, cards);
    }

    static String choixAction(String options) {
        /*
        Demande à l'utilisateur de choisir quoi faire lors de son tour
         */
        if (type.equals("GUI")) return "0";
        else return Input.choixAction(options);
    }

    public static int[] chooseTarget(String card) {
        /*
        Demande à l'utilisateur de choisir une case où bâtir un mur
         */
        if (type.equals("GUI")) return new int[]{0, 0};
        else return Input.chooseTarget(card);
    }

    public static void wallBuilt(String wallType, int[] location) {
        /*
        Indique à l'utilisateur que son mur wallType a bien été bâti aux coordonnées location
         */
        if (type.equals("GUI")) System.out.println("Not implemented yet");
        else Output.wallBuilt(wallType, location);
    }

    public static void message(String action) {
        /*
        Affiche un message (plusieurs possibilités)
         */
        if (type.equals("GUI")) System.out.println("Not implemented yet");
        else Output.message(action);
    }

    public static void message(String action, String card) {
        /*
        Affiche un message (plusieurs possibilités)
         */
        if (type.equals("GUI")) System.out.println("Not implemented yet");
        else Output.message(action, card);
    }

    static void message(int position, int id) {
        if (type.equals("GUI")) System.out.println("Not implemented yet");
        else Output.message(position, id);
    }

    public static void message(String part, int player) {
        /*
        Affiche un message (plusieurs possibilités)
         */
        if (type.equals("GUI")) System.out.println("Not implemented yet");
        else Output.message(part, player);
    }
}
