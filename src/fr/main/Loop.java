package fr.main;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class Loop {

    Loop() {
        // On lance la boucle principale
        int gamemode;
        boolean end = false;

        while (!end) {
        /*
        Fonction principale du programme
         */

            // Lancement du menu
            Interface.displayMenu();
            int choix = Interface.choixMenu();
            if (choix < 5) {
                gamemode = choix - 2;
                // On lance le jeu
                game(gamemode);
            } else if (choix == 5) {
                Interface.switchInterface();
            } else {
                end = true;
            }
        }
    }

    private void game (int gamemode) {
        // On initialise le jeu
        Interface.initialisation();
        boolean endgame = false;
        ArrayDeque<Integer> classement = new ArrayDeque<>();
        String[][] plateau = new String[8][8];
        if (gamemode < 2) {
            plateau[3][0] = "rubis";
            for (int y = 0; y < 8; y++)
                plateau[7][y] = "caisse";
        }
        if (gamemode > 0) {
            plateau[6][0] = "rubis";
            plateau[gamemode - 1][0] = "rubis";
        }
        // On créée les joueurs en fonction du mode de jeu
        ArrayList<Cards> players = new ArrayList<>();
        Map<Integer, int[]> positions = new HashMap<>() {{  // Position de chaque joueur en fonction du mode de jeu
            put(0, new int[]{1, 0, 0});
            put(1, new int[]{5, 3, 2});
            put(2, new int[]{-1, 6, 5});
            put(3, new int[]{-1, -1, 7});
        }};

        for (int i = 0; i < gamemode + 2; i++) {
            plateau[positions.get(i)[gamemode]][7] = "V Joueur " + (i + 1);
            players.add(new Cards(gamemode, i));
        }

        int idPlayer = 0;
        // On fait les tours de jeu jusqu'à la fin de la partie
        while (!endgame) {
            // On fait jouer le joueur
            Interface.message("start turn", idPlayer);  // On affiche le message de début de tour
            Cards player = players.get(idPlayer);
            if (Turn.turn(gamemode, players, plateau, player)) classement.add(player.getId());

            // On vérifie si la partie est finie
            if (classement.size() > gamemode) {
                // On affiche le classement des joueurs
                for (Player play : players) if (play.getIsPlaying()) classement.add(play.getId());
                Interface.message("end");
                for (int i = 0; i < gamemode + 2; i++) if (!classement.isEmpty()) Interface.message(i, classement.poll());
                endgame = true;
            }

            // On attend un peu avant de passer au joueur suivant
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // On passe au prochain joueur n'ayant pas déjà gagné
            do {
                if (idPlayer == gamemode + 1) idPlayer = 0;
                else idPlayer++;
            } while (!players.get(idPlayer).getIsPlaying());
        }
    }
}
