package fr.main;

import java.util.*;

public class Cards extends Player {
    private int cartesManquantes = 5;
    private Map<String, Integer> deck;
    private Map<String, Integer> drawnCards;
    private ArrayDeque<String> program = new ArrayDeque<>();
    private ArrayDeque<String> stock;
    private LinkedList<String> waste = new LinkedList<>();

    public Cards(int gamemode, int number) {
        super(gamemode, number);

        // On lui attribue ses cartes [mur]
        deck = new HashMap<>() {{
            put("avant", 0);
            put("gauche", 0);
            put("droite", 0);
            put("laser", 0);
            put("glace", 2);
            put("pierre", 3);
        }};

        drawnCards = new HashMap<>() {{
            put("avant", 0);
            put("gauche", 0);
            put("droite", 0);
            put("laser", 0);
        }};

        // On créée la pile de carte
        for (int i = 0; i < 18; i++) waste.add("avant");
        for (int i = 0; i < 8; i++) {
            waste.add("gauche");
            waste.add("droite");
        }
        for (int i = 0; i < 3; i++) waste.add("laser");
        fillStock();

        // On lui distribue des cartes au hasard
        fillDeck();
    }

    private void fillStock() {
        Collections.shuffle(waste);
        stock = new ArrayDeque<>(waste);
        waste.clear();
    }

    void discardCard(String card) {
        cartesManquantes++;
        deck.put(card, deck.get(card) - 1);
        waste.add(card);
    }

    void addInstruction(String card) {
        program.add(card);
        cartesManquantes++;
        deck.put(card, deck.get(card) - 1);
    }

    public void buildWall(String type) {
        deck.put(type, deck.get(type) - 1);
    }

    Map<String, Integer> fillDeck() {
        // On vide le tableau des cartes tirées
        drawnCards.put("avant", 0);
        drawnCards.put("gauche", 0);
        drawnCards.put("droite", 0);
        drawnCards.put("laser", 0);

        boolean endStock = false;
        while (cartesManquantes > 0 && !endStock) {
            if (stock.isEmpty() && !waste.isEmpty())
                fillStock();
            if (stock.isEmpty()) {
                endStock = true;
            } else {
                String card = stock.pop();
                deck.put(card, deck.get(card) + 1);
                drawnCards.put(card, drawnCards.get(card) + 1);
                cartesManquantes--;
            }
        }

        return drawnCards;
    }

    boolean inDeck(String card) {
        return deck.get(card) > 0;
    }

    Map<String, Integer> getDeck() {
        return deck;
    }

    String execProg() {
        String card = program.poll();
        if (card == null) {
            if (stock.isEmpty()) fillStock();
            if (cartesManquantes > 0) fillDeck();
        } else {
            waste.add(card);
        }
        return card;
    }
}
