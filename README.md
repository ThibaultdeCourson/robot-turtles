**Ceci est le fichier README. Il automatiquement affiché en dessous du répertoire principal du projet. J'ai mis dedans une partie de la présentation du jeu et, surtout, un tutoriel pour utiliser GitLab.**

# RobotTurtles

# Games rules

## I. main<span></span>.py

On va choisir au hasard le joueur qui va commencer la partie.

A chaque tour, un joueur a le choix entre trois options :

- Compléter le programme ;

- Construire un mur ;

- Exécuter le programme.

- la fin de son tour, et quel que soit l'option choisie, le joueur peut défausser
sa main et piocher des cartes jusqu'à en avoir à nouveau 5.

**exemple** de ce qu'on devra commenter
```shell
println("jeu");
```


# La librairie graphique

Pour faire l'interface graphique nous allons utiliser Swing. Vous pouvez trouver [ici](https://openclassrooms.com/fr/courses/26832-apprenez-a-programmer-en-java/23108-creez-votre-premiere-fenetre) un tutoriel sur son utilisation.




# Use GitLab

In order to contribuate to a project with GitLab (or GitHub it's the same) you need three tools:

1 - An **Internet Browser** to access the GitLab website

2 - The software **Git**, which can be downloaded [here](https://git-scm.com/downloads). It will basically do the link between the GitLab servers and your computer. Git comes with an essential tool for Windows computers : **Git Bash** (you can find it in the Git folder in the applications list). Git Bash offers an improved Linux command prompt which can be used to manipulate Git. Linux and Mac don't need it : they already have a Linux command prompt. If you don't like using commands you can download **GitHub Desktop** which provide a GUI interface to manipulate the local repository. GitHub Desktop works with GitLab servers however it doesn't seems to support SSH communication so you will be forced to use a  less secure HTTP connection.

3 - An IDE where you will code. For a Java project you can use **IntelliJ** or **Eclipse**. In order to easily implement or modify the documentation (this file) you can also use **Visual Studio** (by adding the **Markdown Editor** extension).

## 1 - Use a SSH Key

#### Create a SSH Key:

Open **Git Bash**.

Enter the following command (don't forget to put the email address you used to create your GitLab account).

```shell
ssh-keygen -t rsa -C "your.email@example.com" -b 4096
```

Just press ENTER (leave an empty space) when they ask `Enter file in which to save the key`, enter a password when they ask `Enter passphrase`, do the same a second time.


#### Establish connection with the GitLab server using an SSH key:

Go to the folder where the SSH key has been added (the path is indicated in the console) and open the *id_rsa.pub* file with a Notepad and copy its content.

Go on GitLab's website and enter in *Settings > SSH Keys*, paste the SSH key in the text field, give a name to your SSH Key and click *Add Key*

In Git Bash enter the following command:
```shell
ssh -i $PWD/.ssh/id_rsa git@gitlab.com 
```
Enter your password.

**If you have this type of error:**
```shell
fatal: bad numeric config value 'tru' for 'filter.lfs.required': invalid unit
warning: Clone succeeded, but checkout failed.
```

Then it means that you have a bad configuration. Enter this command :
```shell
rm ~/.gitconfig
```


#### Save your SSH Key password (to not have to write it again at each manipulation)

Enter in Git Bash the following commands: (Note:  can also be used)

``` shell
eval $(ssh-agent)
```

Then, if you are on Mac, use:
``` shell
ssh-add -K
```

If you are on Windows use:
``` shell
git config credential.helper store
```

Enter your password and press ENTER, you should have the message `Identity added:`. Infortunately I still have difficulties to do this manipulation on Windows.


### 2 - Create a local repository of your project:

On the GitLab website, go to the main page of the project Robot Turtles, click on the blue button *clone* and copy the SSH url. It should look like this : `git@gitlab.com:ThibaultdeCourson/robot-turtles.git`

With Git Bash naviguate to the folder you selected for the project (using the bash command `cd "path"`).

**Warning !** The path must not include folders whose name contains spaces. For example the **OneDrive folder** is prohibited : the path `D:\OneDrive - ISEP\mes Documents\Programmes\Java\RobotT` is invalid. 

Enter the command: 
```shell
git clone "url" "name of the project folder"
```
It should look like this : `git clone git@gitlab.com:ThibaultdeCourson/robot-turtles.git RobotTurtles`

Open the folder: you should find the files of the project: *README*, *robot-turtles.iml* ...

**If you have this type of error :** 
``` shell
*** Please tell me who you are.
fatal: unable to auto-detect email address
```

Please run the following commands (don't forget to put your email address and your GitLab pseudo)
``` shell
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

### Good luck !!!!!
